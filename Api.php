<?php

/**
 * Description of API
 *
 * @author jonsnow
 */
require_once("Rest.php");
require_once("Database.php");
require_once("model/ProjectRepository.php");

class API extends REST {

    private $db;
    private $projectRepository;

    public function __construct() {
        parent::__construct();
        $this->db = new Database();
        $this->projectRepository = new ProjectRepository($this->db);
    }

    public function processApi($json) {
        if (!isset($json['student_id'])) {
            throw new Exception('Key student_id does not exist!');
        } else {
            $studentId = $json['student_id'];
        }
        
        $result = $this->projectRepository->read($studentId);

        if (!$result) {
            echo json_encode(array('success' => false));
        } else {
            echo $this->processResult($result, $studentId);
        }
        $this->db->disconnect();
    }

    private function processResult($result, $studentId)
    {
        $resultArr = array('success' => true);
        $resultArr[$studentId] = array();
        
        while ($row = $result->fetch()) {
            $entry = array(
                'course' => 'DBS',
                'assignment' => $row['name'],
                'deadline' => $row['deadline'],
                'submission_date' => $row['submission_date']
            );
            array_push($resultArr[$studentId], $entry);
        }

        return json_encode($resultArr, JSON_UNESCAPED_UNICODE);
    }
    
    /*
     *  Encode array into JSON
     */

    private function json($data) {
        if (is_array($data)) {
            return json_encode($data);
        }
    }

}
