SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `assignments`;
CREATE TABLE `assignments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_slovak_ci NOT NULL,
  `deadline` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

INSERT INTO `assignments` (`id`, `name`, `deadline`) VALUES
(1,	'Správa pamäte',	'2018-05-28 11:28:54'),
(2,	'Sociálne siete',	'2018-05-30 11:28:54'),
(3,	'Popolvár',	'2018-06-01 23:59:59');

DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects` (
  `student_id` int(11) NOT NULL,
  `assignment_id` int(11) NOT NULL,
  `submission_date` datetime DEFAULT NULL,
  KEY `assignment_id` (`assignment_id`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`assignment_id`) REFERENCES `assignments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_slovak_ci;

INSERT INTO `projects` (`student_id`, `assignment_id`, `submission_date`) VALUES
(79791,	1, '2018-06-09 13:44:38'),
(79791,	2, '2018-06-09 13:44:46'),
(79791,	3, '2018-06-09 13:44:52');