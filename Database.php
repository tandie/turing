<?php

/**
 * Description of Database
 *
 * @author jonsnow
 */
class Database {

    private $conn;
    private $host;
    private $username;
    private $password;
    private $dbname;
    private $charset;

    function __constructr() {
        $this->host = "localhost";
        $this->username = "root";
        $this->password = "MauzoleuM147";
        $this->dbname = "turing";
        $this->charset = "utf8";
    }

    function __destruct() {
        $this->disconnect();
    }

    public function getConnection() {

        if ($this->conn) {
            return $this->conn;
        } else {
            $conn = $this->connect();
            return $conn;
        }
    }

    public function connect() {
        if ($this->conn) {
            return $this->conn;
        }
        
        try {
            $this->conn = new PDO(
                    "mysql:host=localhost;dbname=turing;charset=utf8", 
                    "root", "MauzoleuM147");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            die("Connection error: " . $exception->getMessage());
        }

        return $this->conn;
    }

    public function disconnect() {
        if ($this->conn) {
            $this->conn = null;
        }
    }
}
