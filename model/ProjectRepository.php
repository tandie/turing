<?php

/**
 * Description of Project
 *
 * @author jonsnow
 */
class ProjectRepository {

    private $db;
    private $tableName = "projects";
    private $id;
    private $studentId;
    private $assignmentId;
    private $lateDays;

    public function getId() {
        return $this->id;
    }

    public function getStudentId() {
        return $this->studentId;
    }

    public function getAssignmentId() {
        return $this->assignmentId;
    }

    public function getLateDays() {
        return $this->lateDays;
    }

    public function __construct($db) {
        $this->db = $db;
    }

    public function read($id) {

        $conn = $this->db->getConnection();

        if (!$id) {
            return null;
        }

        try {
            $query = 'SELECT a.name, a.deadline, p.submission_date FROM projects p '
                    . 'INNER JOIN assignments a ON a.id = p.assignment_id '
                    . 'WHERE student_id = ?';

            $stmt = $conn->prepare($query);
            $stmt->bindParam(1, $id, PDO::PARAM_INT);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'PDO Error: ' . $e->getMessage() . '<br/>';
            return null;
        }
        return $stmt;
    }

}
