<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once('Api.php');

$api = new API();
$method = 'POST';
$contentType = 'application/json';
$input = "php://input";

if (strcasecmp(filter_input(INPUT_SERVER, 'REQUEST_METHOD'), $method) != 0) {
    throw new Exception('Request method must be post!');
}

$server = filter_input(INPUT_SERVER, 'CONTENT_TYPE');

$type = isset($server) ? trim($server) : '';

if (strcasecmp($type, $contentType) != 0) {
    throw new Exception('Content must be: application/json');
}

$content = trim(file_get_contents($input));

$decoded = json_decode($content, true);

if (!is_array($decoded)) {
    throw new Exception('Received not a valid JSON data!');
}

$api->processApi($decoded);

